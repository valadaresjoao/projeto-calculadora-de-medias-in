const input = document.querySelector(".input");
const boxNotas = document.querySelector(".box-notas");
const exibirMedia = document.querySelector(".exibir-media");

let indexNota = 0;
let notas = 0;
let media = 0;

function aparacerNota() {
    if (input.value === ''){
        alert("Por favor, insira uma nota");
    }
    else if (input.value < 0 || input.value > 10){
        alert("Por favor, digite uma nota válida( 0 - 10 )")
    }
    else {
        let li = document.createElement("li");
        li.setAttribute("id", "nota");
        li.innerHTML = "A nota " + (1 + indexNota) + " foi " + input.value ; 
        boxNotas.appendChild(li);
        indexNota += 1;
        notas += parseFloat(input.value);
        console.log(notas)
        console.log(indexNota)
        console.log(media)
    }
    input.value = '';
}

var btnSubmit = document.querySelector(".btn-submit") ;
btnSubmit.addEventListener("click", aparacerNota);

function calcularMedia() {
    boxNotas.innerHTML = '';
    media = notas/indexNota;
    var novaMedia = document.querySelector(".media-final")
    novaMedia.innerHTML = media;
    indexNota = 0;
    notas = 0;
    media = 0
}

var btnCalcular = document.querySelector(".btn-calcular");
btnCalcular.addEventListener("click", calcularMedia);


